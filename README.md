
# React Redux clean start

*This starter-kit is front end using an hexagonal architecture with a pre configured stack*

React
Redux
Redux selectors
Bootstrap
SASS
Jest

Eslint
Typescript
Prettier
// TODO : Husky hooks 


## The main idea of this architecture :

1. Isolate core and buisness logic from any dependencies and be able to replace a dependencies without any changes in the core
2. Be able to run the application inMemory (without any dependencies)
3. Be able to perfom fast unit tests of the core and business logic


### Inspiration, to go further:


**Blog and articles**
https://wealcomecompany.com/
https://medium.com/ssense-tech/hexagonal-architecture-there-are-always-two-sides-to-every-story-bc0780ed7d9c
https://khalilstemmler.com/
https://medium.com/swlh/inside-out-hexagonal-architecture-in-react-de0642a31e63
https://proandroiddev.com/why-you-need-use-cases-interactors-142e8a6fe576
https://medium.com/codex/clean-architecture-for-dummies-df6561d42c94


**Github repos**
https://github.com/stemmlerjs/ddd-forum
https://github.com/esaraviam/dogappv1


We will use the following simple exemple to illustrate the documentation.
Retrieving a list of user from two differents fake users API jsonPlaceholder and fakeStoreApi


[tl;dr real world example](#real-world-example)



## Glossary 

### Hexagon
The hexagon is the core of the application, it contains all the business logic
 The hexagon is _agnostic, there is no mention to dependies in the hexagon. all depencies should be injected as an depency injection.

### Use Cases
They are the abstract defintion of what the user would like to do in the application.
For exemple : 
- getLongitudeLatitudeFromZipCode
- getTemperatureByCity
- login
- retrievePostsList
- createUser
- sendMessage

 ### Gateways

The gateway manages any contact outside of the process originated by the core, such as accessing the file system or network. Keeping all the specifics for accessing the external service internal to the gateway object is extremely important. If integration details are allowed to leak into the core layer, the benefit of the technique is lost.

In our starterKit, all gateways have a typed interface defined named as ``<Scope>Gateway``
All dependant service will implements the typed gateway and named ``<Dependency><Scope>Gateway``

**Dependency injection**
We will define wich dependency we use in our startupScript file and inject it in our usecase

For exemple a geoGateway interface will have a defined method like 
getPositionByCity(city:string) : {lng:string, lat:string}

Then we will have for each service, a class who implements this gateway like :
``<service><scope>Gateway`` implements ``<Scope>Gateway``
GoogleMapGeoGateway implements geoGateway
BingMapGeoGateway implements geoGateway
...


### Adapters
    
Adapter layer surrond th application core, and only interact with the core. 

*Primary adapters* are drinving adapters, user interaction with the application for exemple react
*Secodary adapters* are drivent adapters, application interaction with external dependencies.

Adapters can be an external API, aJavascript librairie ...
You should be able to add or modify adapters without any changes in the hexagon directory. 
    
> Note : Some dependencies like ramda, lodash, Redux, ... are "core dependencies", because they are generic tools.

For example 
 - React is a primary adapter : you should be able to start an application with an another library like VueJS or
-  If you need to get  longitude and latitude from a zipCode + country. your should be able to use Google Maps API, Bing Maps API, ... they will secondary adapters



### The redux directory
contains : 
All redux logic. Redux is a part of the core
All the reducers, appState and dependencies
The startupScripts.ts file define the gateway switch. For our example witch Gateway will we use (JsonPlaceholder / inMemory / fakeStoreApi)



## Real world example

For our *userList example*, we define first in our hexagon, the user model, the user gateway, and the get users list useCase : 
Indepently from specific API, our app will interact with an API to retrieve a users list.
Without any knowlege of the structure of the API, our app "specs" indicate that we will have to retrive a list of users with an id, a name, an email and a city.

1. **What is a user for our front end ?** 
We define a user model interface
``hexagon/models/user.interface.ts``

```TS
export type User = {
    id: number;
    name: string;
    email: string;
    city: string;
};
```

2. **How our frontend interact with an external adapter ?** 
We define a userGateway, this gateway should retrieve a list of users (typed as the userModel)  

``hexagon/gateways/userGateway.interface.ts``


```TS
import { User } from '../models/user.interface';
[...]
export interface UserGateway {
    requestAllUsers(): Promise<ApiResponse<User[]>>;
}
```


3. **Mocking inMemory users list with json data**

We define our users as a stub : a static collection with some users according to the user model type. this collection will be used for our unit tests or to run the app "in memory"

``adapters/secondary/gateways/inMemory/stubs/someUsers.ts``


```TS
import { User } from  '../../../../../hexagon/models/user.interface';

export  const  someUsers: User[] = [
	{
		id:  1,
		name:  'Hatem',
		email:  'hatem@mail.com',
		city:  'Courbevoie',
	},
	{
		id:  2,
		name:  'Bruno',
		email:  'bruno@mail.com',
		city:  'Grenoble',
	},
// ...

];
```
Then we create a gateway for the inMemory case.

``adapters/secondary/gateways/inMemory/inMemoryUserGateway.ts``

```TS
import { left, right } from  'fp-ts/Either';
import { UserGateway } from  '../../../../hexagon/gateways/userGateway.interface';
import { ApiResponse } from  '../../../../hexagon/infra/ApiResponse';
import { User } from  '../../../../hexagon/models/user.interface';

export  class  InMemoryUserGateway  implements  UserGateway {

	private  usersList = [] as  User[];
	
	requestAllUsers(): Promise<ApiResponse<User[]>> {
			if (this.usersList) {
				return  Promise.resolve(right(this.usersList));
			}
		return  Promise.resolve(left(this.usersList));
	}
	
	feedWith(users: User[]) {
		this.usersList = users;
	}
}
```

Note that this inMemory gateway has a *feedWith* method. We can feed the private usersList array with this method in our startupScript or in a test file.

```TS
// ...
import { someUsers } from  '../adapters/secondary/gateways/inMemory/stubs/someUsers';
// ...
userGateway = new  InMemoryUserGateway();
userGateway.feedWith(someUsers);
```

4. **Define the use case : getUserList**

the use case must be have the user gateway interface as parameter, later we can call it with all the implementations of the user gateway?


``/hexagon/usecases/getUsersList/getUsersListUseCase.ts``

```TS
import { isRight } from  'fp-ts/lib/Either';
import { ThunkResult } from  '../../../redux/configureStore';
import { UserGateway } from  '../../gateways/userGateway.interface';
import  *  as  actionCreators  from  './actionCreators';

export  const  getUsersListUseCase =
	(): ThunkResult<void> =>
	async (dispatch, getState, { userGateway }: { userGateway: UserGateway }) => {
		dispatch(actionCreators.Actions.usersListFetching());
		const  result = await  userGateway.requestAllUsers();
		if (isRight(result)) {
				dispatch(actionCreators.Actions.usersListRetrieved(result.right));
		} else {
				dispatch(actionCreators.Actions.usersListFailed());
		}
	};
```





5. **Testing our use case with the inMemory Data**

We want to test two cases : 
- an empty list 
- a fullfilled list.

First we configure the redux store with our inMemoryGateway.
For testing the empty list we use the feedWith method with an empty array

```TS
import { configureStore, ReduxStore } from '../../../redux/configureStore';
import { getUsersListUseCase } from './getUsersListUseCase';
import { InMemoryUserGateway } from '../../../adapters/secondary/gateways/inMemory/inMemoryUserGateway';
import { AppState } from '../../../redux/appState';
import { someUsers } from '../../../adapters/secondary/gateways/inMemory/stubs/someUsers';

describe('Users list retrieval', () => {
	let store: ReduxStore;
	let userGateway: InMemoryUserGateway;
	let initialState: AppState;

	beforeEach(() => {
		userGateway = new InMemoryUserGateway();
		store = configureStore({ userGateway });
		initialState = store.getState();
	});

	describe('users list is empty', () => {
		beforeEach(() => {
			userGateway.feedWith([]);
		});

		it('should not retrieve any users', async () => {
			await store.dispatch(getUsersListUseCase());
			expect(store.getState()).toEqual({
				...initialState,
				users: {
					status: 'succeeded',
					data: [],
				},
			});
		});
	});

	describe('users list is filled', () => {
		beforeEach(() => {
			userGateway.feedWith(someUsers);
		});

		it('should not retrieve any users', async () => {
			await store.dispatch(getUsersListUseCase());
			expect(store.getState()).toEqual({
				...initialState,
				users: {
					status: 'succeeded',
					data: someUsers,
				},
			});
		});
	});
});
```

Now our app is tested.
We want to retrieve a user list from an real remote API.
For facilty we use fake api without autorization keys :
[JSONPlaceholder](https://jsonplaceholder.typicode.com/) and [Fake Store API](https://fakestoreapi.com/)


### JSONPlaceholder: 

In a jsonPlaceholder  directory, we define first an interface in Typescript, it represents the user model of a *JsonPlaceholder user* as defined in the API route response :
https://jsonplaceholder.typicode.com/users

``adapters/secondary/gateways/jsonPlaceholder/dtos/jsonPlaceholderUserDto.ts``
```TS
export  interface  JsonPlaceholderUserDto {
	id: number;
	name: string;
	username: string;
	email: string;
	address: {
		street: string;
		suite: string;
		city: string;
		zipcode: string;
		geo: {
			lat: string;
			lng: string;
		};
	};
	phone: string;
	website: string;
	company: {
		name: string;
		catchPhrase: string;
		bs: string;
	};
}
```

This user interface is not corresponding to our front userModel, we want to pick the only the id, name, email, city. we define then a "mapper", this mapper take a JsonPlaceholderUserDto as a parametre and return a userModel

```TS
import { JsonPlaceholderUserDto } from  '../dtos/jsonPlaceholderUserDto';
import { User } from  '../../../../../hexagon/models/user.interface';
import { Mapper } from  '../../../../../hexagon/infra/Mapper';

export  class  JsonPlaceholderUserMapper  implements  Mapper<User> {
	static  toModel(userDto: JsonPlaceholderUserDto): User {
		return {
			id:  userDto.id,
			name:  userDto.name,
			email:  userDto.email,
			city:  userDto.address.city,
		};
	}
}
```

Now in an jsonPlaceholder implementation of the use gateway, we will use our mapper and retrieve the json placeholder to our core.

```TS
import { left, right } from  'fp-ts/Either';
import { BaseApi } from  '../../../../hexagon/infra/BaseApi';
import { UserGateway } from  '../../../../hexagon/gateways/userGateway.interface';
import { User } from  '../../../../hexagon/models/user.interface';
import { JsonPlaceholderUserMapper } from  './mappers/jsonPlaceholderUserMapper';
import { JsonPlaceholderUserDto } from  './dtos/jsonPlaceholderUserDto';
import { ApiResponse } from  '../../../../hexagon/infra/ApiResponse';

const  API_BASE_URL = 'https://jsonplaceholder.typicode.com';

export  class  JsonPlaceholderUserGateway  extends  BaseApi  implements  UserGateway {
	async  requestAllUsers(): Promise<ApiResponse<User[]>> {
		try {
			const  response = await  this.get(`${API_BASE_URL}/users`);
			const  users = response.data.map((u: JsonPlaceholderUserDto) =>
				JsonPlaceholderUserMapper.toModel(u),
			);
			return  right(users);
		} catch (error) {
			return  left(error  as  string);
		}
	}
}
``` 

See the fakeStoreApi directory to see an another exemple.

## Available Scripts

In the project directory, you can run:

### `npm run start:inMemory`

Runs the app in the development mode. standalone inMemory version\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


### `npm start`:jsonPlaceholder

Runs the app in the development mode. gateway to JSONplaceholder API (https://jsonplaceholder.typicode.com/) 
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


### `npm start`:Fake Store API

Runs the app in the development mode. gateway to Fake Store API (https://fakestoreapi.com/) 
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.


### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
