import axios, { AxiosError, AxiosInstance } from 'axios';

export abstract class BaseApi {
    protected baseUrl: string | undefined;

    protected bearerToken: string | undefined;

    protected responseType: string;

    protected headersAccept: string;

    private axiosInstance: AxiosInstance | any = null;

    constructor() {
        this.bearerToken = '';
        this.responseType = 'json';
        this.headersAccept = 'application/json';
        this.axiosInstance = axios.create({});
        this.enableInterceptors();
    }

    private enableInterceptors(): void {
        this.axiosInstance.interceptors.response.use(
            this.getSuccessResponseHandler(),
            (err: AxiosError) => this.getErrorResponseHandler(err),
        );
    }

    private getSuccessResponseHandler() {
        return (response: AxiosError) => response;
    }

    private getErrorResponseHandler(err: AxiosError) {
        if (err.response?.status === 404) {
            throw new Error(`${err.config.url} not found`);
        }
        throw err;
    }

    public setResponseType(responseType: string) {
        this.responseType = responseType;
    }

    public setHeadersAccept(headersAccept: string) {
        this.headersAccept = headersAccept;
    }

    protected async get(url: string, params?: any): Promise<any> {
        return this.axiosInstance({
            method: 'GET',
            url,
            params: params || null,
            responseType: this.responseType,
        });
    }

    protected async post(url: string, data?: any, params?: any): Promise<any> {
        return this.axiosInstance({
            method: 'POST',
            url,
            data: data || null,
            params: params || null,
        });
    }
}
