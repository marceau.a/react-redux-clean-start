import { ApiResponse } from '../infra/ApiResponse';
import { User } from '../models/user.interface';

export interface UserGateway {
    requestAllUsers(): Promise<ApiResponse<User[]>>;
    requestAddUser(user: Omit<User, 'id'>): Promise<ApiResponse<number>>;
}
