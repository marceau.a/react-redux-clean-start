import { ActionsUnion, createAction } from '../../../redux/customActions';
import { User } from '../../models/user.interface';

export const Actions = {
    userAdding: () => createAction('users/userAddRequested'),
    userAddFailed: () => createAction('users/userAddFailed'),
    userAdded: (user: User) => createAction('users/userAdded', { user }),
};

export type ActionsType = ActionsUnion<typeof Actions>;
