import { isRight } from 'fp-ts/lib/Either';
import { ThunkResult } from '../../../redux/configureStore';
import { UserGateway } from '../../gateways/userGateway.interface';
import { User } from '../../models/user.interface';
import * as actionCreators from './actionCreators';

export const addUserUseCase =
    (user: Omit<User, 'id'>): ThunkResult<void> =>
    async (dispatch, getState, { userGateway }: { userGateway: UserGateway }) => {
        dispatch(actionCreators.Actions.userAdding());

        const result = await userGateway.requestAddUser(user);

        if (isRight(result)) {
            dispatch(actionCreators.Actions.userAdded({ id: result.right, ...user }));
        }
    };
