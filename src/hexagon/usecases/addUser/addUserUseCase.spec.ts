import { configureStore, ReduxStore } from '../../../redux/configureStore';

import { InMemoryUserGateway } from '../../../adapters/secondary/gateways/inMemory/inMemoryUserGateway';
import { someUsers } from '../../../adapters/secondary/gateways/inMemory/stubs/someUsers';
import { addUserUseCase } from './addUserUseCase';

describe('Adding a user', () => {
    let store: ReduxStore;
    let userGateway: InMemoryUserGateway;

    beforeEach(() => {
        userGateway = new InMemoryUserGateway();
        store = configureStore({ userGateway });
        userGateway.feedWith(someUsers);
    });

    it('should add a user and retrieve it in store with an id', async () => {
        await store.dispatch(addUserUseCase({ name: 'A', email: 'B', city: 'C' }));

        expect(store.getState().users.data).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    id: expect.any(Number),
                    name: 'A',
                    email: 'B',
                    city: 'C',
                }),
            ]),
        );
    });
});
