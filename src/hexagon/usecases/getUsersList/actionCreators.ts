import { ActionsUnion, createAction } from '../../../redux/customActions';
import { User } from '../../models/user.interface';

export const Actions = {
    usersListFetching: () => createAction('users/usersRequested'),
    usersListFailed: () => createAction('users/usersRequestFailed'),
    usersListRetrieved: (users: User[]) => createAction('users/usersReceived', { users }),
};

export type ActionsType = ActionsUnion<typeof Actions>;
