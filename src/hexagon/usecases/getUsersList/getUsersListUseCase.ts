import { isRight } from 'fp-ts/lib/Either';
import { ThunkResult } from '../../../redux/configureStore';
import { UserGateway } from '../../gateways/userGateway.interface';
import * as actionCreators from './actionCreators';

export const getUsersListUseCase =
    (): ThunkResult<void> =>
    async (dispatch, getState, { userGateway }: { userGateway: UserGateway }) => {
        dispatch(actionCreators.Actions.usersListFetching());
        const result = await userGateway.requestAllUsers();
        if (isRight(result)) {
            dispatch(actionCreators.Actions.usersListRetrieved(result.right));
        } else {
            dispatch(actionCreators.Actions.usersListFailed());
        }
    };
