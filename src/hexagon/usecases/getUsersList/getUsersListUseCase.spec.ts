import { configureStore, ReduxStore } from '../../../redux/configureStore';
import { getUsersListUseCase } from './getUsersListUseCase';

import { InMemoryUserGateway } from '../../../adapters/secondary/gateways/inMemory/inMemoryUserGateway';
import { AppState } from '../../../redux/appState';
import { someUsers } from '../../../adapters/secondary/gateways/inMemory/stubs/someUsers';

describe('Users list retrieval', () => {
    let store: ReduxStore;
    let userGateway: InMemoryUserGateway;
    let initialState: AppState;

    beforeEach(() => {
        userGateway = new InMemoryUserGateway();
        store = configureStore({ userGateway });
        initialState = store.getState();
    });

    describe('users list is empty', () => {
        beforeEach(() => {
            userGateway.feedWith([]);
        });
        it('should not retrieve any users', async () => {
            await store.dispatch(getUsersListUseCase());
            expect(store.getState()).toEqual({
                ...initialState,
                users: {
                    status: 'succeeded',
                    data: [],
                },
            });
        });
    });

    describe('users list is filled', () => {
        beforeEach(() => {
            userGateway.feedWith(someUsers);
        });
        it('should not retrieve any users', async () => {
            await store.dispatch(getUsersListUseCase());
            expect(store.getState()).toEqual({
                ...initialState,
                users: {
                    status: 'succeeded',
                    data: someUsers,
                },
            });
        });
    });
});
