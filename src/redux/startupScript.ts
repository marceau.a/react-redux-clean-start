import { configureStore } from './configureStore';

import { InMemoryUserGateway } from '../adapters/secondary/gateways/inMemory/inMemoryUserGateway';
import { someUsers } from '../adapters/secondary/gateways/inMemory/stubs/someUsers';
import { JsonPlaceholderUserGateway } from '../adapters/secondary/gateways/jsonPlaceholder/jsonPlaceholderUserGateway';
import { FakeStoreApiUserGateway } from '../adapters/secondary/gateways/fakeStoreApi/fakeStoreApiUserGateway';

let userGateway;

switch (process.env.REACT_APP_SOURCE) {
    default:
    case 'inMemory':
        userGateway = new InMemoryUserGateway();
        userGateway.feedWith(someUsers);
        break;

    case 'jsonPlaceholder':
        userGateway = new JsonPlaceholderUserGateway();
        break;

    case 'fakeStoreApi':
        userGateway = new FakeStoreApiUserGateway();
        break;
}

const store = configureStore({
    userGateway,
});

export default store;
