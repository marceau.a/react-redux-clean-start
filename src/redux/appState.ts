import { User } from '../hexagon/models/user.interface';

export type FetchStatus = 'idle' | 'loading' | 'succeeded' | 'failed';

export interface AppState {
    users: {
        status: FetchStatus;
        data: User[];
    };
}
