import { AnyAction, combineReducers } from 'redux';
import produce from 'immer';
import { User } from '../../hexagon/models/user.interface';
import { FetchStatus } from '../appState';

export const data = (state: User[] = [], action: AnyAction) => {
    if (action.type === 'users/usersReceived') return action.payload.users;
    if (action.type === 'users/userAdded') {
        // Using immer
        const nextState = produce(state, (draftState) => {
            draftState.push(action.payload.user);
        });
        return nextState;
    }
    return state;
};

export const status = (state: FetchStatus = 'idle', action: AnyAction) => {
    if (action.type === 'users/usersRequested') return 'loading';
    if (action.type === 'users/usersReceived') return 'succeeded';
    if (action.type === 'users/usersRequestFailed') return 'failed';

    if (action.type === 'users/userAddRequested') return 'loading';
    if (action.type === 'users/userAdded') return 'succeeded';
    if (action.type === 'users/userAddFailed') return 'failed';

    return state;
};

export const usersReducer = combineReducers({
    status,
    data,
});
