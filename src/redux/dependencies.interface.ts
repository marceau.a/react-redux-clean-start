import { UserGateway } from '../hexagon/gateways/userGateway.interface';

export interface Dependencies {
    userGateway: UserGateway;
}
