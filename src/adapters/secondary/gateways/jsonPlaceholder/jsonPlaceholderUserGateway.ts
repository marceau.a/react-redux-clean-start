import { left, right } from 'fp-ts/Either';

import { BaseApi } from '../../../../hexagon/infra/BaseApi';
import { UserGateway } from '../../../../hexagon/gateways/userGateway.interface';
import { User } from '../../../../hexagon/models/user.interface';
import { JsonPlaceholderUserMapper } from './mappers/jsonPlaceholderUserMapper';
import { JsonPlaceholderUserDto } from './dtos/jsonPlaceholderUserDto';
import { ApiResponse } from '../../../../hexagon/infra/ApiResponse';

const API_BASE_URL = 'https://jsonplaceholder.typicode.com';

export class JsonPlaceholderUserGateway extends BaseApi implements UserGateway {
    async requestAllUsers(): Promise<ApiResponse<User[]>> {
        try {
            const response = await this.get(`${API_BASE_URL}/users`);
            const users = response.data.map((u: JsonPlaceholderUserDto) =>
                JsonPlaceholderUserMapper.toModel(u),
            );
            return right(users);
        } catch (error) {
            return left(error as string);
        }
    }

    async requestAddUser(user: Omit<User, 'id'>): Promise<ApiResponse<number>> {
        try {
            const response = await this.post(`${API_BASE_URL}/users`, { user });
            const { id } = response.data;
            return right(id);
        } catch (error) {
            return left(error as string);
        }
    }
}
