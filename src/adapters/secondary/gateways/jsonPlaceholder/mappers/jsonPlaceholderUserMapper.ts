import { JsonPlaceholderUserDto } from '../dtos/jsonPlaceholderUserDto';
import { User } from '../../../../../hexagon/models/user.interface';
import { Mapper } from '../../../../../hexagon/infra/Mapper';

export class JsonPlaceholderUserMapper implements Mapper<User> {
    static toModel(userDto: JsonPlaceholderUserDto): User {
        return {
            id: userDto.id,
            name: userDto.name,
            email: userDto.email,
            city: userDto.address.city,
        };
    }
}
