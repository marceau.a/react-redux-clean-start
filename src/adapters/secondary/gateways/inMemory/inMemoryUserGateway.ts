import { left, right } from 'fp-ts/Either';
import { UserGateway } from '../../../../hexagon/gateways/userGateway.interface';
import { ApiResponse } from '../../../../hexagon/infra/ApiResponse';
import { User } from '../../../../hexagon/models/user.interface';

export class InMemoryUserGateway implements UserGateway {
    private usersList = [] as User[];

    requestAllUsers(): Promise<ApiResponse<User[]>> {
        if (this.usersList) {
            return Promise.resolve(right(this.usersList));
        }
        return Promise.resolve(left(this.usersList));
    }

    requestAddUser(user: Omit<User, 'id'>): Promise<ApiResponse<number>> {
        return Promise.resolve(right(Math.floor(Math.random() * 100)));
    }

    feedWith(users: User[]) {
        this.usersList = users;
    }
}
