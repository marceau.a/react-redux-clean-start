import { User } from '../../../../../hexagon/models/user.interface';

export const someUsers: User[] = [
    {
        id: 1,
        name: 'Hatem',
        email: 'hatem@mail.com',
        city: 'Courbevoie',
    },
    {
        id: 2,
        name: 'Bruno',
        email: 'bruno@mail.com',
        city: 'Grenoble',
    },
    {
        id: 3,
        name: 'Zak',
        email: 'zak@mail.com',
        city: 'Courbevoie',
    },
    {
        id: 4,
        name: 'Marceau',
        email: 'marceau@mail.com',
        city: 'Marseille',
    },
];
