import { FakeStoreUserDto } from '../dtos/fakeStoreUserDto';
import { User } from '../../../../../hexagon/models/user.interface';
import { Mapper } from '../../../../../hexagon/infra/Mapper';

export class FakeStoreUserMapper implements Mapper<User> {
    static toModel(userDto: FakeStoreUserDto): User {
        return {
            id: userDto.id,
            name: `${userDto.name.firstname} ${userDto.name.lastname}`,
            email: userDto.email,
            city: userDto.address.city,
        };
    }
}
