import { left, right } from 'fp-ts/Either';

import { BaseApi } from '../../../../hexagon/infra/BaseApi';
import { UserGateway } from '../../../../hexagon/gateways/userGateway.interface';
import { User } from '../../../../hexagon/models/user.interface';
import { FakeStoreUserMapper } from './mappers/fakeStoreUserMapper';
import { FakeStoreUserDto } from './dtos/fakeStoreUserDto';
import { ApiResponse } from '../../../../hexagon/infra/ApiResponse';

const API_BASE_URL = 'https://fakestoreapi.com';

export class FakeStoreApiUserGateway extends BaseApi implements UserGateway {
    async requestAllUsers(): Promise<ApiResponse<User[]>> {
        try {
            const response = await this.get(`${API_BASE_URL}/users`);
            const users = response.data.map((u: FakeStoreUserDto) =>
                FakeStoreUserMapper.toModel(u),
            );
            return right(users);
        } catch (error) {
            return left(error as string);
        }
    }

    async requestAddUser(user: Omit<User, 'id'>): Promise<ApiResponse<number>> {
        try {
            return right(Date.now()); // faking last insterted id for this example cause this api has no fake POST create user
        } catch (error) {
            return left(error as string);
        }
    }
}
