import { User } from '../../../hexagon/models/user.interface';
import { AppState, FetchStatus } from '../../../redux/appState';

export interface UserVM {
    status: FetchStatus;
    data: User[];
    count: number;
}

export const getUsersSelector = (state: AppState): UserVM => ({
    status: state.users.status,
    data: state.users.data,
    count: state.users.data.length,
});
