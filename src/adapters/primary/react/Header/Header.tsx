import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { Navbar, NavbarBrand, Nav, NavbarText } from 'reactstrap';
import { getUsersSelector } from '../../view-models-generators/usersSelectors';

const Header: FunctionComponent = () => {
    const { count } = useSelector(getUsersSelector);

    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand>Dummy users list</NavbarBrand>
            <Nav className="mr-auto" navbar />
            <NavbarText>
                {count} {count === 1 ? 'user' : 'users'}
            </NavbarText>
        </Navbar>
    );
};

export default Header;
