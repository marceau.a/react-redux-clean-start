import React, { FunctionComponent, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Label, Button, FormGroup } from 'reactstrap';
import { addUserUseCase } from '../../../../hexagon/usecases/addUser/addUserUseCase';
import { getUsersSelector } from '../../view-models-generators/usersSelectors';

const UserForm: FunctionComponent = () => {
    const dispatch = useDispatch();

    const { status } = useSelector(getUsersSelector);

    const [name, setName] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [city, setCity] = useState<string>('');

    const canSubmit = [name, email, city].every(Boolean);

    const handleSubmitForm = () => {
        dispatch(addUserUseCase({ name, email, city }));
        setName('');
        setEmail('');
        setCity('');
    };

    return (
        <div className="mb-4">
            <h2>Add a user</h2>
            <FormGroup>
                <Label for="name">Name</Label>
                <Input
                    type="text"
                    name="name"
                    id="name"
                    value={name}
                    onChange={(e) => setName(e.currentTarget.value)}
                />
            </FormGroup>
            <FormGroup>
                <Label for="emaim">Email</Label>
                <Input
                    type="email"
                    name="email"
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.currentTarget.value)}
                />
            </FormGroup>
            <FormGroup>
                <Label for="city">City</Label>
                <Input
                    type="text"
                    name="city"
                    id="city"
                    value={city}
                    onChange={(e) => setCity(e.currentTarget.value)}
                />
            </FormGroup>

            {canSubmit && (
                <Button
                    color="primary"
                    disabled={status === 'loading'}
                    block
                    onClick={handleSubmitForm}
                >
                    Add
                </Button>
            )}
        </div>
    );
};

export default UserForm;
