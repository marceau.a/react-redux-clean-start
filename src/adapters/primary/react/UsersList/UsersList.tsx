import React, { FunctionComponent, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Spinner, Table, Alert } from 'reactstrap';
import { getUsersListUseCase } from '../../../../hexagon/usecases/getUsersList/getUsersListUseCase';
import { getUsersSelector } from '../../view-models-generators/usersSelectors';

const UsersList: FunctionComponent = () => {
    const dispatch = useDispatch();
    const { data, status, count } = useSelector(getUsersSelector);

    useEffect(() => {
        dispatch(getUsersListUseCase());
    }, [dispatch]);

    return (
        <>
            <h2>
                Users List ({count}) {status === 'loading' && <Spinner />}
            </h2>

            {status === 'failed' ? (
                <Alert color="danger">something went wrong!</Alert>
            ) : (
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>City</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((u) => (
                            <tr key={u.id}>
                                <th scope="row">{u.id}</th>
                                <td>{u.name}</td>
                                <td>{u.email}</td>
                                <td>{u.city}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            )}
        </>
    );
};

export default UsersList;
