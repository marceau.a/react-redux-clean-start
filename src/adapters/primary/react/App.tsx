import React, { FunctionComponent } from 'react';
import '../../../app.scss';
import { Container, Row, Col } from 'reactstrap';
import Header from './Header/Header';
import UsersList from './UsersList/UsersList';
import UserForm from './UserForm/UserForm';

const App: FunctionComponent = () => (
    <>
        <Header />
        <Container fluid>
            <Row>
                <Col xs={12} sm={6} lg={4} xl={3}>
                    <UserForm />
                </Col>
                <Col xs={12} sm={6} lg={8} xl={9}>
                    <UsersList />
                </Col>
            </Row>
        </Container>
    </>
);

export default App;
